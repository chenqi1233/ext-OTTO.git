//% color="#4169E1" iconWidth=50 iconHeight=40
namespace OTTO{
  
    //% block="OTTOInit PIN_YL [PIN_YL]PIN_YR [PIN_YR]PIN_RL [PIN_RL]PIN_RR[PIN_RR]PIN_Buzzer[PIN_Buzzer]PIN_Trigger[PIN_Trigger]PIN_Echo[PIN_Echo]   " blockType="command" 
    //% PIN_YL.shadow="dropdown" PIN_YL.options="PIN_YL"
    //% PIN_YR.shadow="dropdown" PIN_YR.options="PIN_YR"
    //% PIN_RL.shadow="dropdown" PIN_RL.options="PIN_RL"
    //% PIN_RR.shadow="dropdown" PIN_RR.options="PIN_RR"
    //% PIN_Buzzer.shadow="dropdown" PIN_Buzzer.options="PIN_Buzzer"
    //% PIN_Trigger.shadow="dropdown" PIN_Trigger.options="PIN_Trigger"
    //% PIN_Echo.shadow="dropdown" PIN_Echo.options="PIN_Echo"
    export function OTTOInit(parameter: any, block: any) {
         let yl=parameter.PIN_YL.code;
         let yr=parameter.PIN_YR.code;
         let rl=parameter.PIN_RL.code;
         let rr=parameter.PIN_RR.code;
         let buzzer=parameter.PIN_Buzzer.code;
         let trigger=parameter.PIN_Trigger.code;
         let echo=parameter.PIN_Echo.code;
     

        Generator.addInclude("OTTOInit", "#include <Otto9.h>");    
        Generator.addObject("OTTOInit","Otto9 ",`otto1;`);  
        Generator.addSetup(`OTTOInit`, `otto1.init(${yl}, ${yr}, ${rl}, ${rr}, true, A6, ${buzzer}, ${trigger}, ${echo});`);
        
        
    }

    //% block="OTTOInit calibration YL[YL]YR[YR]RL[RL]RR[RR]    " blockType="command" 
    //% YL.shadow="number" YL.defl="0"  
    //% YR.shadow="number" YR.defl="0"  
    //% RL.shadow="number" RL.defl="0"  
    //% RR.shadow="number" RR.defl="0"  
 
    export function OTTOInitset(parameter: any, block: any) {
        let yl=parameter.YL.code;
        let yr=parameter.YR.code;
        let rl=parameter.RL.code;
        let rr=parameter.RR.code;
         
       Generator.addSetup(`OTTOInit1`, `otto1.setTrims(${yl},${yr},${rl},${rr});`);
   }

    //% block="OTTO Motion[MOTION]Speed[SPEED]    " blockType="command" 
    //% MOTION.shadow="dropdown" MOTION.options="MOTION"  
    //% SPEED.shadow="dropdown" SPEED.options="SPEED"  
    
    export function OTTOInitsport(parameter: any, block: any) {
        let motion=parameter.MOTION.code;
        let speed=parameter.SPEED.code;
         
        
     if(`${motion}` === '1'){
        Generator.addCode( `otto1.walk(1,${speed},1); `);
        

     } else if(`${motion}` === '2'){
        Generator.addCode( `otto1.walk(1,${speed},-1); `);
    
     } else if(`${motion}` === '3'){
        Generator.addCode( `otto1.turn(1,${speed},-1); `);
        

     } else if(`${motion}` === '4'){
        Generator.addCode( `otto1.turn(1,${speed},1); `);
        

     }else if(`${motion}` === '5'){
        Generator.addCode( `otto1.bend(1,${speed},-1); `);
        

     } else if(`${motion}` === '6'){
        Generator.addCode( `otto1.bend(1,${speed},1); `);
        

     }else if(`${motion}` === '7'){
        Generator.addCode( `otto1.shakeLeg(1,${speed},-1); `);
        

     } else if(`${motion}` === '8'){
        Generator.addCode( `otto1.shakeLeg(1,${speed},1); `);
        

     }
         
       
   }
       //% block="OTTO Dance [DANCE]Speed [SPEED]Size [SIZE]    " blockType="command" 
    //% DANCE.shadow="dropdown" DANCE.options="DANCE"  
    //% SPEED.shadow="dropdown" SPEED.options="SPEED"  
    //% SIZE.shadow="dropdown" SIZE.options="SIZE" 
    export function OTTOInitdance(parameter: any, block: any) {
        let dance=parameter.DANCE.code;
        let speed=parameter.SPEED.code;
        let size=parameter.SIZE.code;

        if(`${dance}` === '1'){
            Generator.addCode( `otto1.moonwalker(1, ${speed}, ${size}, -1);`);
            
    
         } else if(`${dance}` === '2'){
            Generator.addCode( `otto1.moonwalker(1, ${speed}, ${size}, 1);`);
        
         } else if(`${dance}` === '3'){
            Generator.addCode( `otto1.crusaito(1, ${speed}, ${size}, -1);`);
            
    
         } else if(`${dance}` === '4'){
            Generator.addCode( `otto1.crusaito(1, ${speed}, ${size}, 1);`);
            
    
         }else if(`${dance}` === '5'){
            Generator.addCode( `otto1.flapping(1, ${speed}, ${size}, -1);`);
            
    
         } else if(`${dance}` === '6'){
            Generator.addCode( `otto1.flapping(1, ${speed}, ${size}, 1);`);
            
    
         } 
       
   }
    //% block="OTTO Action [ACTION]Speed [SPEED]Size [SIZE]    " blockType="command" 
    //% ACTION.shadow="dropdown" ACTION.options="ACTION"  
    //% SPEED.shadow="dropdown" SPEED.options="SPEED"  
    //% SIZE.shadow="dropdown" SIZE.options="SIZE" 
    export function OTTOInitjump(parameter: any, block: any) {
        let action=parameter.ACTION.code;
        let speed=parameter.SPEED.code;
        let size=parameter.SIZE.code;
         
       Generator.addCode( `otto1.${action}(1, ${speed}, ${size});`);
   }
    //% block="OTTO Sound [SOUND]  " blockType="command" 
    //% SOUND.shadow="dropdown" SOUND.options="SOUND"  
    
    export function OTTOInitsound(parameter: any, block: any) {
        let sound=parameter.SOUND.code;
         
         
       Generator.addCode( `otto1.sing(${sound});`);
   }
    //% block="OTTO Post [POST]  " blockType="command" 
    //% POST.shadow="dropdown" POST.options="POST"  
    
    export function OTTOInitzitai(parameter: any, block: any) {
        let post=parameter.POST.code;
         
         
       Generator.addCode( `otto1.playGesture(${post});`);
   }
}
